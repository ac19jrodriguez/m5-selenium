/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boxes.impl;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author Jordi
 */
public class NewClass extends Application {
    @Override
	public void start(final Stage primaryStage) throws Exception {
        
        final int width = 540;
        final int height = 610;
        final int margen = 20;
        int count = 0;
        final double MAX_FONT_SIZE = 30.0; // TAMAÑO SCORES DE PLAYERS

        
        
	Canvas canvas = new Canvas(width, height);
        
    	// Obtenir Graphics Contextsx
    	final GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(5, 40, 50, 50);
        gc.fillRect(495, 40, 50, 50);
        gc.fillRect(3, 115, 495, 425);
        
        int distanciaB = 5;
        int distanciaAlt = 120;
        
        gc.setFill(Color.RED);
//        gc.fillRect(15, 130, 90, 110);
//        
//        gc.fillRect(115, 130, 95, 110);
//        
//        gc.fillRect(220, 130, 95, 110);
//        
        
        for (int i = 1; i < 5; i++) {
                        for (int j = 1; j < 7; j++) {
                        gc.fillRect(distanciaB+10, distanciaAlt+10, 70, 90);
                        distanciaB += 80;
                        }
                        distanciaB = 5;
                        distanciaAlt += 100;
                        
        }
        distanciaB = 5;
        distanciaAlt = 120;
        
        
        for (int i = 1; i < 5; i++) {
                        for (int j = 1; j < 8; j++) {
                            gc.setFill(Color.GREEN);
                           
                            gc.fillRect(distanciaB, distanciaAlt, 10, 110);
                            distanciaB += 80;
                        }
                        distanciaB = 5;
                        distanciaAlt += 100;
        }        
        
        distanciaB = 5;
        distanciaAlt = 120;
        
        for (int i = 1; i < 6; i++) {
                        for (int j = 1; j < 7; j++) {
                            gc.setFill(Color.GREEN);
                           
                            gc.fillRect(distanciaB, distanciaAlt, 80, 10);
                            distanciaB += 80;
                        }
                        distanciaB = 5;
                        distanciaAlt += 100;
        } 
        
        
        distanciaB = 5;
        distanciaAlt = 120;
        
        
    	 for (int i = 1; i < 6; i++) {
                        for (int j = 1; j < 8; j++) {
                            gc.setFill(Color.BLACK);
                            
                            gc.fillRect(distanciaB, distanciaAlt, 10, 10);
                            distanciaB += 80;
                            
                        }
                        distanciaB = 5;
                        distanciaAlt += 100;
                        count = 0;
        }        
        
        
         
        Label p1 = new Label();
        p1.setPadding(new Insets(10,10,10,10));
        
        p1.setText("0");
        p1.setFont(new Font(MAX_FONT_SIZE)); 
        
        Label p2 = new Label();
        p2.setPadding(new Insets(10,10,10,10));
        
        p2.setText("0");
        p2.setFont(new Font(MAX_FONT_SIZE)); 
       

    	
        StackPane root = new StackPane();
        root.setStyle("-fx-padding: 10;"
                     +"-fx-border-style: solid inside;"
                     +"-fx-borderwidth: 2;"
                     +"-fx-border-radius: 5;"
                     +"-fx-border-color:black;"
                     +"-fx-background-color:grey;"  
        );
        root.getChildren().addAll(canvas);
        root.getChildren().addAll(p1,p2);
        StackPane.setAlignment(p1, Pos.TOP_LEFT);
        StackPane.setAlignment(p2, Pos.TOP_RIGHT);
        
      
        
//        Pane GridPane Hbox
        
    
    	
    	Scene scene = new Scene(root, 550, 600);
    	primaryStage.setTitle("Exemple Canvas");
    	primaryStage.setScene(scene);
    	primaryStage.show();
	}
        
	public static void main(String[] args) {
    	launch(args);
	}

//Torna la distància entre els punts (x1,y1) i (x2,y2)
	private  double distancia(double x1, double y1, double x2, double y2) {
    	return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
	}

   

}
