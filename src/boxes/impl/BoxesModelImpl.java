package boxes.impl;

import java.util.HashSet;
import java.util.Set;

import boxes.BoxesContract.BoxesModel;
import boxes.BoxesContract.BoxesModelListener;
import boxes.Edge;

public class BoxesModelImpl implements BoxesModel {

	private int size;
	private int turn; // 0=off, 1=player1, 2=player2
	private int remaining;
	private int score[];
	private int hori[][], vert[][], fill[][];
	private Set<BoxesModelListener> listeners;

	public BoxesModelImpl(int size) {		
		this.size = size;
		this.turn = 0;
		this.listeners = new HashSet<>();
	}
	
	@Override
	public int getSize() {
		return size;
	}
	
	@Override
	public boolean addListener(BoxesModelListener listener) {
		return listeners.add(listener);
	}

	@Override
	public boolean removeListener(BoxesModelListener listener) {
		return listeners.remove(listener);
	}

	@Override
	public void start(int turn) {	
                throw new RuntimeException("no implementat!");
	}
	
	@Override
	public int getWinner() {		
                throw new RuntimeException("no implementat!");
	}
	
	@Override
	public int getTurn() {
		return turn;
	}
	
	@Override
	public int getScore(int player) {
		return score[player-1];
	}
	
	@Override
	public int getBox(int x, int y) {
		throw new RuntimeException("no implementat!");
	}
	
	@Override
	public int getEdge(Edge e) {
		throw new RuntimeException("no implementat!");
	}
	
	@Override
	public boolean play(int player, Edge e) {	
		throw new RuntimeException("no implementat!");
	}
	
	@Override
	public boolean isOver() {
		throw new RuntimeException("no implementat!");
	}
	
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		for (int y=0; y<size-1; y++) {
			for (int x=0; x<size-1; x++) {
				sb.append(" ");
				sb.append(hori[x][y] != 0? "-":" ");
			}			
			sb.append(" \n");
			for (int x=0; x<size-1; x++) {
				sb.append(vert[x][y] != 0? "|":" ");
				sb.append(fill[x][y] != 0? Integer.toString(fill[x][y]):" ");
			}
			sb.append(vert[size-1][y] != 0? "|":" ");
			sb.append("\n");
		}
		for (int x=0; x<size-1; x++) {
			sb.append(" ");
			sb.append(hori[x][size-1] != 0? "-":" ");
		}
		sb.append(" \n");
		
		return sb.toString();
	}

}
